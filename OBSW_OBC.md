- Identify major needs and issues - what are the next steps?
  - State of the repos
  - Is there any design? Are the requirements for it known?

- Main devs? Main knowledge gaps?
  - Embedded development

- How will we share the work


Future steps:
 - Issues in GitLab (https://gitlab.com/acubesat/obc/obc-software/-/issues) (assign this)
 - Create a high and low level design (assign this)
 - For other subsystems:
    - Drivers
    - ECSS
    - SpaceCAN
    - Reviewing procedure
    - Setting up CI/CD
