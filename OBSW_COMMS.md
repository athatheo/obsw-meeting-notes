- Identify major needs and issues - what are the next steps?
  - State of the repos
    Nothing major.

  - Is there any design? Are the requirements for it known?

- Main devs? Main knowledge gaps?
  - CLion
  - Embedded
  - CI/CP
  - Testing
  - GNU Radio knowledge (including python)

- How will we share the work?




Goals:
COMMS SW:
Ground Station SW:
  - Encoders/Decoders for the ground station; ready stuff from LSF or from GNU Radio.
HMAT-> Digital Signature generation and verification

On-Board SW:
  - Possible contribution to LSF board.
  - https://gitlab.com/librespacefoundation/satnogs-comms
  - https://gitlab.com/librespacefoundation/fpga-cores

Future Tasks:
 - Investigate progress from librespacefoundation (assign kongr, efthymia)
    - Possibilities: redo, contribute, etc.


-----------------------------------------------------------------------------

LSF progress - Meeting 12-03-21


- Baptiste expressed interest for frame synchronization
- Meeting with LSF to discuss:
  - What will they be doing or will be starting shortly
    They'll make a library in CPP. They use ETL. They'll have free resources.
    In two weeks they will have FPGA on the board. In two weeks, asking for task timeline.    
  - What should we begin with?
  - Do they have any resources/plans/architecture/design

In case of not doing FPGA, do you think feasible for an MCU

Solid so far: AT86RF215, RFCC

https://gitlab.com/groups/librespacefoundation/ic-drivers/-/shared
  - What should we begin with or should we coordinate with them somehow?
  - Do they have any resources/plans/architecture/design?

-----------------------------------------------------------------------------
COMMS Meeting - 14-03-21

- What is COMMS?
CCSDS -> COMMS Board that will receive messages from the antenna (Antenna -> Transceiver -> Board)
FPGA or not? Can we do it without? ESA actually said that we might not need it.
AT86RF215 driver (it's the transceiver)
LDPC -> error correction for payload data (for onboard we care only about the encoder)
BCH -> error correction for TC packets
HMAC -> Digital Signature (so that only we can send critical stuff)
Peripherals -> Transceiver
