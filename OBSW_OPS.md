- Identify major needs and issues - what are the next steps?
  - State of the repos
  - Is there any design? Are the requirements for it known?

- Main devs? Main knowledge gaps?
  - JavaScript
  - databases
  - python
  - system Integration

- How will we share the work
  - OBC and new recruits.


Goals:
 - Mission control interface https://yamcs.org/ https://public.ccsds.org/Pubs/660x1g1.pdf https://gitlab.com/philipbrack/xtcetools
 - TM/TC databases
 - Integration with GNU Radio (collaboration with COMMS)
 - Pass scheduler


 Future Tasks:
 - Position for SW OPS (assign task to kozaris)
 - Investigate the goals and what is needed for that (assign to me)
