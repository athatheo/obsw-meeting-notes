- What **is** this meeting?
  - Helping us understand needs, coz we need to organize this.
  - Generic meeting.
  - Our main responsibility: to create tasks.
  - What are our next steps?
    - 1-on-1s
    - Training
    - Coordination and communication between SSs.
    - Deadlines for all that shit will be strict and in the quite-near future
      ~to avoid becoming the shittiest subsystem in AcubeSAT; SU.~
    - Delegate the shit out of everything to members.


- Plan for software development training
  - Git(Lab), FreeRTOS, Basics on embedded, IDE, Testing(Logic, Unit), Documentation
  - Who is going to help?
  - Task: Identify each subsystem's weakness in sw dev?


- 1-on-1 meetings with each subsystem
  - Which are the subsystems that will develop code?
  - Identify major needs and issues - what are the next steps?
    - State of the repos
    - Change the code or redo?
    - Is there any design? Are the requirements for it known?
  - Main devs? Main knowledge gaps?
  - How will we share the work?


- Plan for major deadlines (internal and external)
  - What needs to be done by MRR?
  - Do we have any internal deadlines? Do we need to create any?


- Tasks for generic stuff that is needed for every SW dev. subteam
  - Review checklist
  - Codestyle (preferably scripted & automated)
  - Documentation procedure
  - Increase activity in Software channel
