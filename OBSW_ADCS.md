- Identify major needs and issues - what are the next steps?
  - State of the repos
  - Is there any design? Are the requirements for it known?

- Main devs? Main knowledge gaps?
  - Every member (accountable Romanos)
  - Major knowledge gaps on everything

- How will we share the work?
  - OBC(ECSS Services)
  - OBC&ADCS(Drivers)
  - ADCS (Business logic)

Goals:
  - ECSS Services
  - Drivers
  - Mathematics

Next steps:
  - Mathematics -> translate from Matlab to C++ (Matlab Coder??)
  - Check albedo for C++ (assign task)
  - Magnetorquers, magnetometer, reaction wheel (investigate drivers for this)
